var socket;

if (!window.WebSocket) {
	window.WebSocket = window.MozWebSocket;
}

if (window.WebSocket) {
	var path;

	ajax.getNoAsync('chat/im-server', '', function(res) {
		let data = res.data
		let ssl = "ws"
		if (data.ssl == true) {
			ssl = "wss"
		}
		path = ssl + "://" + "localhost" + ":" + data.port + data.wsPath + "/" + token
	})
	socket = new WebSocket(path);
	socket.onopen = function(event) {
		console.log(event.type)
		setInterval(refreshAuth, 10000)
	};
	socket.onclose = function(event) {
		console.log(event.type)
		socket = new WebSocket(path);
	};
} else {
	alert("Your browser does not support Web Socket.");
}

function refreshAuth() {
	var msg = {
		'chatType': 0,
		'msgType': 0,
		'from': curUser.id,
		'fromNickname': curUser.nickname,
	};
	socket.send(JSON.stringify(msg));
}
