var index = (function() {
	function init() {
		$("#add").click(function() {
			var $model = $('#my-prompt');
			$model.modal({
				relatedElement: this,
				onConfirm: function(data) {
					ajax.post('user/add-friend', {
						'uid': data.data
					}, function(res) {
						$model.modal('close');
						// $("#alert-content").text("已发出请求");
						// $("#my-alert").modal();
					})
				},
				onCancel: function() {}
			});
		});

		$("#create_group").click(function() {
			var $model = $("#my-popup");

			ajax.post('user/friends', {
				'pageNum': 0,
				'pageSize': 10
			}, function(res) {
				let html = "";
				for (e of res.data.list) {
					html += '<li>\n' + '<label class="am-checkbox">\n' +
						e.nickname +
						'<input name="fids" type="checkbox" value="' + e.id +
						'" data-am-ucheck>\n' +
						'</label></li>\n';
				}
				$('#group_friend').html(html);
				$model.modal({
					relatedElement: this,
					height: 300,
					onConfirm: function() {
						var ids = [];
						$('#group_friend input:checkbox').each(function() {
							if ($(this).is(":checked")) {
								ids.push($(this).val());
							}
						});
						var gname = $('#group_name').val();
						if (gname == null || gname == '' || gname == undefined) {
							$model.modal('close');
							$("#alert-content").text("群组名不能为空");
							$("#my-alert").modal();
						} else if (ids.length == 0) {
							$model.modal('close');
							$("#alert-content").text("群组为空，无法建立群组");
							$("#my-alert").modal();
						} else {
							ajax.post('group/create', {
								members: ids,
								name: gname
							}, function(res) {
								console.log("create-group " + res)
							});
							$model.modal('close');
							chat.initFriendsList()
						}
					}
				})
			})
		});

		var faceBox;
		$("body").on('click', 'a.face-item', function(e) {
			var content = "face[" + $(this).attr("title") + "]";
			var text = document.getElementById('input_box');
			text.value = text.value + content;
			layer.close(faceBox);
		});

		$("#face").click(function() {
			var $face = $("#face").offset();
			var top = $face.top - 200;
			var left = $face.left - 28;
			faceBox = layer.open({
				type: 1,
				title: false,
				closeBtn: 0,
				shade: 0.00001,
				shadeClose: true,
				offset: [top, left],
				content: $("#face-list").html(),
				area: ['400px', '190px']
			})
		});

	}

	var pad = function pad(num, n) {
		var len = num.toString().length;
		while (len < n) {
			num = "0" + num;
			len++;
		}
		return num;
	};

	return {
		init: init,
	};
})();
