var ajax = (function() {
	function post(url, data, callback) {
		$.ajax({
			url: api_addr + url,
			data: JSON.stringify(data),
			type: "post",
			dataType: "json",
			contentType: "application/json;charset=utf-8",
			beforeSend: function(xhr) {
				xhr.setRequestHeader('token', token)
			},
			success: function(data, status) {
				if (data.code == 0) {
					callback(data)
				} else if (data.code == 1001) {
					location.href = "login.html"
				} else {
					layer.msg(data.msg)
				}
			},
			error: function(xhr, status, error) {
				console.log(xhr.responseJSON)
				layer.msg("服务器错误")
			}
		})
	}

	function get(url, data, callback) {
		$.ajax({
			url: api_addr + url,
			data: data,
			type: "get",
			dataType: "json",
			contentType: "application/json;charset=utf-8",
			beforeSend: function(xhr) {
				xhr.setRequestHeader('token', token)
			},
			success: function(data, status) {
				if (data.code == 0) {
					callback(data)
				} else if (data.code == 1001) {
					location.href = "login.html"
				} else {
					layer.msg(data.msg)
				}
			},
			error: function(xhr, status, error) {
				console.log(xhr.responseJSON)
				layer.msg("服务器错误")
			}
		})
	}

	function getNoAsync(url, data, callback) {
		$.ajax({
			url: api_addr + url,
			data: data,
			type: "get",
			async: false,
			dataType: "json",
			contentType: "application/json;charset=utf-8",
			beforeSend: function(xhr) {
				xhr.setRequestHeader('token', token)
			},
			success: function(data, status) {
				if (data.code == 0) {
					callback(data)
				} else if (data.code == 1001) {
					location.href = "login.html"
				} else {
					layer.msg(data.msg)
				}
			},
			error: function(xhr, status, error) {
				console.log(xhr.responseJSON)
				layer.msg("服务器错误")
			}
		})
	}

	return {
		post: post,
		get: get,
		getNoAsync: getNoAsync
	}
})();
