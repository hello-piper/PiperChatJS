var token = (function() {
	return sessionStorage.getItem("token")
})()

function setToken(token) {
	sessionStorage.setItem("token", token)
}

var curUser = (function() {
	return JSON.parse(sessionStorage.getItem("localUser"))
})()

function setUserInfo(user) {
	sessionStorage.setItem("localUser", JSON.stringify(user))
}
